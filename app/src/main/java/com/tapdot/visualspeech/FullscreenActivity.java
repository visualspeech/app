package com.tapdot.visualspeech;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Toast;

import java.util.Arrays;

public class FullscreenActivity extends AppCompatActivity {
    private CameraManager cameraManager;
    private TextureView cameraView;
    private CameraDevice cameraDevice;
    private String cameraID;
    private Size dimensions;
    private CaptureRequest captureRequest;
    private CaptureRequest.Builder captureRequestBuilder;
    private CameraCaptureSession cameraCaptureSessions;
    private Handler cameraHandler;
    private HandlerThread cameraBackgroundThread;
    private ImageReader CVHook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        cameraView = findViewById(R.id.cameraView);
        assert cameraView != null;
        cameraView.setSurfaceTextureListener(textureListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        startBackgroundThread();
        if(cameraView.isAvailable()) {
            openCamera();
        } else {
            cameraView.setSurfaceTextureListener(textureListener);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        closeCamera();
        stopBackgroundThread();
    }

    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            Log.e("CAMERATEST", "camera opened");
            cameraDevice = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };

    private void updatePreview() {
        if(cameraDevice == null) {
            Log.e("CAMERATEST", "updatePreview error, return");
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {

            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, cameraHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1337) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(this, "You must grant camera permissions to use this app.", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    private void openCamera() {
        CameraManager mgr = (CameraManager)getSystemService(Context.CAMERA_SERVICE);
        try {
            cameraID = mgr.getCameraIdList()[0];
            assert cameraID != null;
            CameraCharacteristics characteristics = mgr.getCameraCharacteristics(cameraID);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            dimensions = map.getOutputSizes(SurfaceTexture.class)[0];
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1337);
                return;
            }

            mgr.openCamera(cameraID, stateCallback, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeCamera() {
        if(cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    private void createCameraPreview() {
        try {
            SurfaceTexture tx = cameraView.getSurfaceTexture();
            assert tx != null;
            tx.setDefaultBufferSize(dimensions.getWidth(), dimensions.getHeight());
            final Surface surface = new Surface(tx);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            captureRequestBuilder.addTarget(surface);
            CVHook = ImageReader.newInstance(dimensions.getWidth(), dimensions.getHeight(), PixelFormat.RGBA_8888, 2);
            CVHook.setOnImageAvailableListener(new Processor(), null);
            captureRequestBuilder.addTarget(CVHook.getSurface());
            cameraDevice.createCaptureSession(Arrays.asList(surface, CVHook.getSurface()), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    if(cameraDevice == null) {
                        return;
                    }
                    cameraCaptureSessions = session;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    Toast.makeText(getApplicationContext(), "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, cameraHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopBackgroundThread() {
        cameraBackgroundThread.quitSafely();
        try {
            cameraBackgroundThread.join();
            cameraBackgroundThread = null;
            cameraHandler = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startBackgroundThread() {
        cameraBackgroundThread = new HandlerThread("Camera Background");
        cameraBackgroundThread.start();
        cameraHandler = new Handler(cameraBackgroundThread.getLooper());
    }

    private TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };
}

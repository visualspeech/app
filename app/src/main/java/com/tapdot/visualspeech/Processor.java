package com.tapdot.visualspeech;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.Image;
import android.media.ImageReader;
import android.util.Log;

import java.nio.ByteBuffer;

public class Processor implements ImageReader.OnImageAvailableListener {
    private BitmapFactory.Options decodeOpts = new BitmapFactory.Options();
    @Override
    public void onImageAvailable(ImageReader reader) {
        decodeOpts.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Image image = reader.acquireLatestImage();
        int width = image.getWidth();
        int height = image.getHeight();

        int pixelStride = image.getPlanes()[0].getPixelStride();
        int rowStride = image.getPlanes()[0].getRowStride();
        int rowPadding = rowStride - pixelStride * width;

        Bitmap bitmap = Bitmap.createBitmap(
                width + rowPadding / pixelStride, height,
                Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(image.getPlanes()[0].getBuffer());
        image.close();

        Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, width, height, null, true);
        Log.d("epically", Integer.toString(Color.red(bmp.getPixel(0, 0))));
        image.close();
    }
}
